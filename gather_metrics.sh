#!/bin/sh

BUILD_FOLDER_SIZE="$(du -h -s build | sed 's/\s.*$//')";
BUNDLE_SIZE_METRIC_LINE="bundle_size $BUILD_FOLDER_SIZE";

METRICS=$(cat <<-END
$BUNDLE_SIZE_METRIC_LINE
END
)

echo "$METRICS" > metrics.txt

1. [Testing React Redux with Jest and Enzyme](https://github.com/simpletut/Testing-React-Redux-with-Jest-and-Enzyme)

2. [React Jest und Enzyme YouTube Video Tutorial](https://www.youtube.com/redirect?redir_token=pWXjwaDRgpCiEuc2As8brI0hYIl8MTU2NTE2NzU4M0AxNTY1MDgxMTgz&event=video_description&v=92F8_9UG04g&q=https%3A%2F%2Fgithub.com%2Fsimpletut%2FTesting-React-Redux-with-Jest-and-Enzyme)

3. [CRA configure ESLint](https://medium.com/@leandroaps/best-eslintrc-configuration-for-cra-for-now-81753cf39c19)

4. [CRA setting up Editor](https://facebook.github.io/create-react-app/docs/setting-up-your-editor)

5. [CRA advanced configuration](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#advanced-configuration)

6. [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/introduction.html#gitlab-pages-requirements)

7. [GitLab CI with Vuejs](https://about.gitlab.com/2017/09/12/vuejs-app-gitlab/)
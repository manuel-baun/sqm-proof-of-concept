const async = require('async');
const fs = require('fs');
const path = require('path');

const getSize = (item, callback) => {
	let totalSize = 0;
	fs.lstat(item, (err, stats) => {
		if (err) return callback(err);

		if (stats.isDirectory()) {
			fs.readdir(item, (err, list) => {
				if (err) return callback(err);

				async.each(
					list,
					(listItem, cb) => {
						getSize(path.join(item, listItem), (err, size) => {
							totalSize += size;
							cb();
						});
					},
					(err) => {
						if (err) return callback(err);
						callback(null, totalSize);
					}
				);
			});
		} else {
			// Ensure fully asynchronous API
			process.nextTick(function() {
				callback(null, (totalSize += stats.size));
			});
		}
	});
};

const folderToMeasure = process.argv[2];
getSize(folderToMeasure, (err, totalSize) => {
	// const fileNameToSave = './' + process.argv[3];
	// console.log(folderToMeasure, fileNameToSave);
	if (!err) {
		let res = 'total_size: ' + totalSize / 1000 + 'Kb\n';
		console.log(res);

		// if (fs.existsSync(fileNameToSave)) {
		// 	fs.appendFile(fileNameToSave, res, function(err) {
		// 		if (err) return console.error(err);
		// 	});
		// } else {
		// 	fs.writeFile(fileNameToSave, res, function(err) {
		// 		if (err) return console.error(err);
		// 	});
		// }
	}
});

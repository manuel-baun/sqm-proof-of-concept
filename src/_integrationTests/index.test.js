import moxios from 'moxios';
import { testStore } from '../../utils';
import { fetchPosts } from './../actions';

describe('fetchPosts action', () => {
	beforeEach(() => {
		moxios.install();
	});

	afterEach(() => {
		moxios.uninstall();
	});

	test('Store is updated correctly', () => {
		const expectedState = [
			{
				title: 'Example Title 1',
				body: 'Test 1'
			},
			{
				title: 'Example Title 2',
				body: 'Test 2'
			},
			{
				title: 'Example Title 3',
				body: 'Test 3'
			}
		];

		const store = testStore();

		moxios.wait(() => {
			const request = moxios.requests.mostRecent();
			request.respondWith({
				status: 200,
				response: expectedState
			});
		});

		return store.dispatch(fetchPosts()).then(() => {
			const newState = store.getState();
			expect(newState.posts).toBe(expectedState);
		});
	});
});

import React, { Component } from 'react';
import { connect } from 'react-redux';

import Header from './components/header/index';
import Headline from './components/headline/index';
import CustomButton from './components/button/index';
import ListItem from './components/list_item/index';
import { fetchPosts } from './actions';
import './app.scss';

import moment from 'moment';

const users = [
	{
		fName: 'Max',
		lName: 'Mustermann',
		email: 'max.mustermann@gmail.com',
		age: 25,
		onlineStatus: true
	}
];

const not_needed = moment();
console.info(not_needed.format('dd-mm-yy'));

// // Eslint should complain
// const variableNotBeingUsed = 'I am not used';
// const variableNotBeingUsed2 = 'I am not used';
// const variableNotBeingUsed3 = '3';

const initialState = {
	hideButton: false
};

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			...initialState
		};
		this.fetch = this.fetch.bind(this);
	}

	fetch() {
		this.props.fetchPosts();
		this.exampleMethod_updateState();
	}

	exampleMethod_updateState() {
		const { hideButton } = this.state;
		this.setState({
			hideButton: !hideButton
		});
	}

	//  Method for a test
	exampleMethod_returnsAValue(number) {
		return number + 1;
	}

	render() {
		const { posts } = this.props;
		const { hideButton } = this.state;

		const configButton = {
			buttonText: 'Get Posts',
			emitEvent: this.fetch
		};

		return (
			<div className="App" data-test="appComponent">
				<Header className="App-header" />
				<section className="main">
					<Headline header="Posts" desc="Click here to get some random posts" user={users} />
					{!hideButton && <CustomButton {...configButton} />}
					{posts.length > 0 && (
						<div>
							{posts.map((post, index) => {
								const { title, body: desc } = post;
								return <ListItem key={index} title={title} desc={desc} />;
							})}
						</div>
					)}
				</section>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		posts: state.posts
	};
};

export default connect(mapStateToProps, { fetchPosts })(App);

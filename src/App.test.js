import React from 'react';
import App from './App';
import { shallow } from 'enzyme';
import { findByTestAttr, testStore } from './../utils/index';

const setUp = (initialState = {}) => {
	const store = testStore(initialState);
	const wrapper = shallow(<App store={store} />).childAt(0).dive();
	// console.log(wrapper);
	return wrapper;
};

describe('App Component', () => {
	let wrapper;
	beforeEach(() => {
		const initialState = {
			posts: [
				{
					title: 'Example title 1',
					body: 'Some Text'
				},
				{
					title: 'Example title 2',
					body: 'Some Text'
				},
				{
					title: 'Example title 3',
					body: 'Some Text'
				}
			]
		};
		wrapper = setUp(initialState);
	});

	it('Should render without errors', () => {
		const component = findByTestAttr(wrapper, 'appComponent');
		expect(component.length).toBe(1);
	});

	// Tests methods
	it('exampleMethod_updateState Method should update state as expected', () => {
		const classInstance = wrapper.instance();
		classInstance.exampleMethod_updateState();
		const newState = classInstance.state.hideButton;
		expect(newState).toBe(true);
	});

	it('exampleMethod_returnAValue Method should return value as expected', () => {
		const classInstance = wrapper.instance();
		const newValue = classInstance.exampleMethod_returnsAValue(10);

		expect(newValue).toBe(11);
	});

	it('This test is just for testing purpose and does not test anything else', () => {
		const testValue = 10;

		expect(testValue).toBe(10);
	});
});

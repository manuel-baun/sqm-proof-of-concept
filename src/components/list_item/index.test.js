import React from 'react';
import { shallow } from 'enzyme';
import { findByTestAttr, checkProps } from '../../../utils/index';
import ListItem from './index';

describe('CustomButton Component', () => {
	describe('Checking PropTypes', () => {
		it('Should not throw a warning', () => {
			const expectedProps = {
				title: 'Example ListComponent Title',
				desc: 'Example ListComponent desc'
			};
			const propsError = checkProps(ListItem, expectedProps);
			expect(propsError).toBeUndefined();
		});
	});

	describe('Component Renders', () => {
		let wrapper;
		beforeEach(() => {
			const props = {
				title: 'Example ListComponent Title',
				desc: 'Example ListComponent desc'
			};
			wrapper = shallow(<ListItem {...props} />);
		});

		it('Should Render without errors', () => {
			const component = findByTestAttr(wrapper, 'listItemComponent');
			expect(component.length).toBe(1);
		});

		it('Should Render a title', () => {
			const component = findByTestAttr(wrapper, 'listItemComponentTitle');
			expect(component.length).toBe(1);
		});

		it('Should Render a desc', () => {
			const component = findByTestAttr(wrapper, 'listItemComponentDesc');
			expect(component.length).toBe(1);
		});
	});

	describe('Component should not Render', () => {
		let wrapper;
		beforeEach(() => {
			const props = {
				desc: 'Example ListComponent desc'
			};
			wrapper = shallow(<ListItem {...props} />);
		});

		it('component not rendered', () => {
			const component = findByTestAttr(wrapper, 'listItemComponent');
			expect(component.length).toBe(0);
		});
	});
});

import React from 'react';
import './styles.scss';

const Header = (props) => {
	return (
		<header data-test="headerComponent">
			<h1 data-test="title">SQM Proof of Concept</h1>
		</header>
	);
};

export default Header;

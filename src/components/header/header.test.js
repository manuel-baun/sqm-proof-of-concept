import React from 'react';
import { shallow } from 'enzyme';
import Header from './index';
import { findByTestAttr } from './../../../utils/index';

const setUp = (props = {}) => {
	const component = shallow(<Header {...props} />);
	return component;
};

describe('Header Component', () => {
	let component;
	beforeEach(() => {
		component = setUp();
	});

	it('should render header without errors', () => {
		const wrapper = findByTestAttr(component, 'headerComponent');
		expect(wrapper.length).toBe(1);
	});

	it('should render wrap without errors', () => {
		const wrapper = findByTestAttr(component, 'title');
		expect(wrapper.length).toBe(1);
	});
});

import React from 'react';
import { findByTestAttr, checkProps } from '../../../utils/index';
import CustomButton from './index';
import { shallow } from 'enzyme';

describe('CustomButton Component', () => {
	describe('Checking PropTypes', () => {
		it('Should not throw a warning', () => {
			const expectedProps = {
				buttonText: 'Example Button Text',
				emitEvent: () => {}
			};
			const propsError = checkProps(CustomButton, expectedProps);
			expect(propsError).toBeUndefined();
		});
	});

	describe('Component Renders', () => {
		let wrapper;
		let mockFunc;
		beforeEach(() => {
			mockFunc = jest.fn();
			const props = {
				buttonText: 'Example Button Text',
				emitEvent: mockFunc
			};
			wrapper = shallow(<CustomButton {...props} />);
		});

		it('Should Render a button', () => {
			const component = findByTestAttr(wrapper, 'buttonComponent');
			expect(component.length).toBe(1);
		});

		// Simulate Event clicks
		it('Should emit callback on click event', () => {
			const component = findByTestAttr(wrapper, 'buttonComponent');
			component.simulate('click');
			const callback = mockFunc.mock.calls.length;
			expect(callback).toBe(1);
		});
	});
});

import React from 'react';
import { shallow } from 'enzyme';
import Headline from './index';

import { findByTestAttr, checkProps } from './../../../utils/index';

const setUp = (props = {}) => {
	const component = shallow(<Headline {...props} />);
	return component;
};

describe('Headline Component', () => {
	describe('Checking PropTypes', () => {
		it('Should not throw a warning', () => {
			const expectedProps = {
				header: 'Test Header',
				desc: 'Test desc',
				user: [
					{
						fName: 'Test fName',
						lName: 'Test lName',
						email: 'test@gmail.com',
						age: 25,
						onlineStatus: false
					}
				]
			};
			const propsErr = checkProps(Headline, expectedProps);

			expect(propsErr).toBeUndefined();
		});
	});

	describe('Has props', () => {
		let wrapper;
		beforeEach(() => {
			const props = { header: 'Test Header', desc: 'Test desc' };
			wrapper = setUp(props);
		});

		it('Should render without error', () => {
			const component = findByTestAttr(wrapper, 'HeadlineComponent');
			expect(component.length).toBe(1);
		});

		it('Should render a h1', () => {
			const component = findByTestAttr(wrapper, 'header');
			expect(component.length).toBe(1);
		});

		it('Should render a desc', () => {
			const component = findByTestAttr(wrapper, 'descWrapper');
			expect(component.length).toBe(1);
		});
	});

	describe('Has NO Props', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = setUp();
		});

		it('Should not render', () => {
			const component = findByTestAttr(wrapper, 'HeadlineComponent');
			expect(component.length).toBe(0);
		});
	});
});

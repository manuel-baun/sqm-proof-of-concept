# Installationsanleitung
### Nodejs und React
1. Nodejs von der [Nodejs-Homepage](https://nodejs.org/en/download/) herunterladen
2. Die heruntergeladene Datei installieren
3. Die Gezippte Projektdatei entpacken
4. Terminal im Ordner öffnen, indem auch die `package.json` liegt
5. Mit `npm install` werden alle Projektabhängigkeiten installiert
6. Anschließend kann mit `npm start` die Webapp gebaut werden. Ist der Prozess durchlaufen, wird ein Browserfenster geöffnet und die Webapp wird angezeigt.

### Tests ausführen
Es können verschiedene Befehle zum Ausführen der Tests verwendet werden:
- Mit `npm run test` wird das Testframework `Jest` gestartet. Anschließend kann mittels CLI das Framework bedient werden.
- Mit `npm run test:coverage` werden alle Tests durchgeführt und die Ergebnisse in dem Terminal angezeigt.
- Mit `npm run test:ci` werden ebenfalls alle Tests durchgeführt. Dieser Befehlt führt zusätzlich `jest-junit` aus. Es wird ein Ordner `coverage` erstellt, in dem alle Testergebnisse enthalten sind.
Unter `coverage\locv-report` befindet sich die generierte Webseite, die alle Ergebnisse darstellt. Zum Starten muss die `index.html` mit einem Browser geöffnet werden.  

